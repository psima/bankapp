package ithak.jpaexample.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Konto {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique=true,updatable = false, nullable = false)
    private String iban;

    @NotNull
    private String bic = "IBOB00001";

    @Column(precision = 10, scale = 2)
    private double guthaben;

    @OneToOne
    private User user;

    @OneToMany(mappedBy = "senderKonto", cascade = CascadeType.ALL)
    private List<Ueberweisung> ueberweisung;

    @OneToMany(mappedBy = "empfaengerKonto", cascade = CascadeType.ALL)
    private List<Ueberweisung> empfangeneUeberweisung;
}
