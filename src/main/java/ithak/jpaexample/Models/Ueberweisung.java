package ithak.jpaexample.Models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Ueberweisung {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String empfaenger;

    @NotNull
    private String sender;

    @NotNull
    private String empfaengerIban;

    @NotNull
    private String senderIban;

    @NotNull
    @Column(precision = 2)
    private double betrag;

    @Column(name = "datum", nullable = false)
    @GeneratedValue
    private Timestamp datum = new Timestamp(System.currentTimeMillis());

    @NotNull
    private String verwendungszweck;

    @ManyToOne
    @JsonIgnore
    private Konto senderKonto;

    @ManyToOne
    @JsonIgnore
    private Konto empfaengerKonto;

}
