package ithak.jpaexample.Config;

import ithak.jpaexample.Security.JwtConfigurer;
import ithak.jpaexample.Security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/auth/signin").permitAll()
                .antMatchers(HttpMethod.GET, "/user").hasRole("ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/konto/**").permitAll()
                .antMatchers(HttpMethod.GET,"/user/**").hasAnyRole("ANGESTELLT", "USER")
                .antMatchers(HttpMethod.POST, "/user").permitAll()
                .antMatchers(HttpMethod.PUT, "/user").hasAnyRole("ANGESTELLT", "USER")
                .antMatchers(HttpMethod.DELETE, "/user/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungen").hasRole("ANGESTELLT")
                .antMatchers(HttpMethod.POST, "/ueberweisung").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungDate/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungVz/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungSender/**").hasRole("ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungEmpfaenger/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungEIban/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungSIban/**").hasRole("ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungByDate/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.GET, "/ueberweisungByBetrag/**").hasAnyRole("USER", "ANGESTELLT")
                .antMatchers(HttpMethod.POST, "/einzahlung/**").hasAnyRole("KASSA")
                .antMatchers(HttpMethod.POST, "/auszahlung/**").hasAnyRole("KASSA")
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
        //@formatter:on
    }
}
