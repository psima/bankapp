package ithak.jpaexample;

import ithak.jpaexample.Models.Bank;
import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Models.Ueberweisung;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Repositories.BankRepository;
import ithak.jpaexample.Repositories.KontoRepository;
import ithak.jpaexample.Repositories.UeberweisungRepository;
import ithak.jpaexample.Repositories.UserRepository;
import ithak.jpaexample.Services.UeberweisungService;
import ithak.jpaexample.Services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;


@Component
@Slf4j
@SpringBootApplication
public class JpaexampleApplication {

    @Autowired
    UserRepository users;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    BankRepository bank;


    @Bean
    PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }


    public static void main(String[] args) {
        SpringApplication.run(JpaexampleApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(UserService userService, UserRepository userRepository, BankRepository bankRepository, UeberweisungRepository ueberweisungRepository, KontoRepository kontoRepository, UeberweisungService ueberweisungService) {
        //soll beim Start zum Bootstrapping ausgeführt werden.
        return (args) -> {

            Bank bank = new Bank();
            bankRepository.save(bank);


            this.users.save(User.builder()
                    .username("admin")
                    .password(this.passwordEncoder.encode("password"))
                    .roles(Arrays.asList("ROLE_ANGESTELLT"))
                    .verfuegernummer(userService.createVerfuegernummer())
                    .bank(bank)
                    .build()
            );

            this.users.save(User.builder()
                    .username("Angestellter")
                    .password(this.passwordEncoder.encode("password"))
                    .roles(Arrays.asList("ROLE_USER", "ROLE_ANGESTELLT"))
                    .verfuegernummer(userService.createVerfuegernummer())
                    .bank(bank)
                    .build()
            );

            this.users.save(User.builder()
                    .username("Kassa")
                    .password(this.passwordEncoder.encode("password"))
                    .roles(Arrays.asList("ROLE_KASSA"))
                    .verfuegernummer(userService.createVerfuegernummer())
                    .bank(bank)
                    .build()
            );


            User u0 = new User();
            u0.setUsername("Winston");
            u0.setPassword("abc");
            u0.setRoles(Arrays.asList("ROLE_USER"));
            u0.setBank(bank);
            u0.setVerfuegernummer(userService.createVerfuegernummer());

            Konto konto1 = new Konto();
            konto1.setIban("xyz");

            userRepository.save(u0);
            konto1.setUser(u0);
            kontoRepository.save(konto1);

            User u1 = new User();
            u1.setUsername("Philipp");
            u1.setPassword("12345");
            u1.setVerfuegernummer(11111);
            u1.setBank(bank);
            u1.setRoles(Arrays.asList("ROLE_USER"));
            Konto konto2 = new Konto();
            konto2.setIban("abc");
            userRepository.save(u1);
            konto2.setUser(u1);
            kontoRepository.save(konto2);

            Ueberweisung ueberweisung = new Ueberweisung();
            ueberweisung.setVerwendungszweck("test");
            ueberweisung.setEmpfaengerIban("xyz");
            ueberweisung.setEmpfaenger("Winston");
            ueberweisung.setSender("Philipp");
            ueberweisung.setSenderIban("abc");
            ueberweisung.setBetrag(1000);
            ueberweisung.setSenderKonto(konto2);
            ueberweisung.setEmpfaengerKonto(konto1);
            ueberweisungRepository.save(ueberweisung);

            Ueberweisung ueberweisung2 = new Ueberweisung();
            ueberweisung2.setVerwendungszweck("testController");
            ueberweisung2.setEmpfaengerIban("abc");
            ueberweisung2.setEmpfaenger("Philipp");
            ueberweisung2.setSender("Winston");
            ueberweisung2.setSenderIban("xyz");
            ueberweisung2.setBetrag(1000);
            ueberweisung2.setSenderKonto(konto1);
            ueberweisung2.setEmpfaengerKonto(konto2);
            ueberweisungRepository.save(ueberweisung2);

//            ueberweisungService.einzahlung((long)1, 50);
//            ueberweisungService.auszahlung((long)1, 500);
        };
    }
}

