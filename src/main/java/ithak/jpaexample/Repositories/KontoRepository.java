package ithak.jpaexample.Repositories;

import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KontoRepository extends JpaRepository<Konto, Long> {
    Optional<Konto> findKontoByIban(String i);
    Optional<Konto> findKontoByIbanAndUser(String i, User u);
    Optional<Konto> findKontoByUser(User u);
}
