package ithak.jpaexample.Repositories;

import ithak.jpaexample.Models.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankRepository extends JpaRepository<Bank, Long> {
    Bank findByBankName(String bankName);
}
