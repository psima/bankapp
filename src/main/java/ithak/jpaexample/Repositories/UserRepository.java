package ithak.jpaexample.Repositories;

import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Models.Bank;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findUserByBank(Bank b);
    Optional<User> findByUsername(String kundenName);
    User findByusernameAndVerfuegernummer(String kundeName, int verfuegernummer);
    Optional<User> findUserByVerfuegernummer(int v);
    User findUserByKonto(Konto k);
}
