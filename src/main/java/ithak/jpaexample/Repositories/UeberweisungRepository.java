package ithak.jpaexample.Repositories;

import ithak.jpaexample.Models.Ueberweisung;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface UeberweisungRepository extends JpaRepository<Ueberweisung, Long> {
    List<Ueberweisung> findUeberweisungBySender(String sender);
    List<Ueberweisung> findUeberweisungByDatum(Date d);
    List<Ueberweisung> findUeberweisungByEmpfaenger(String empfaenger);
    List<Ueberweisung> findUeberweisungByVerwendungszweck(String verwendungszweck);
    List<Ueberweisung> findUeberweisungByEmpfaengerIban(String empfaengerIban);
    List<Ueberweisung> findUeberweisungByDatumBetween(Date date, Date date2);
    List<Ueberweisung> findUeberweisungBySenderIban(String senderIban);

}
