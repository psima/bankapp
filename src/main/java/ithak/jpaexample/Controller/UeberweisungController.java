package ithak.jpaexample.Controller;


import ithak.jpaexample.Exceptions.*;
import ithak.jpaexample.Models.Ueberweisung;
import ithak.jpaexample.Services.UeberweisungService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UeberweisungController {

    private UeberweisungService ueberweisungService;

    public UeberweisungController(UeberweisungService ueberweisungService) {
        this.ueberweisungService = ueberweisungService;
    }

    @GetMapping("/ueberweisungen")
    public ResponseEntity<List<Ueberweisung>> getAllUeberweisung() {return ResponseEntity.ok(ueberweisungService.getAllUeberweisung());}

    @PostMapping("/ueberweisung")
    public ResponseEntity<Ueberweisung> addUeberweisung(@RequestBody @Valid Ueberweisung ueberweisung, BindingResult bindingResult) throws UeberweisungNotValidException, UeberweisungDuplicateId, KontoNotFoundException {
        if (bindingResult.hasErrors()) {
            String message = "";
            for (FieldError fe : bindingResult.getFieldErrors()) {
                message += fe.getField() + "; " + fe.getDefaultMessage();
            }
            throw new UeberweisungNotValidException(message);
        } else {
            return ResponseEntity.ok(ueberweisungService.addUeberweisung(ueberweisung));
        }
    }

    @GetMapping("/ueberweisungId/{id}")
    public ResponseEntity<Ueberweisung> getUeberweisungById(@PathVariable Long id) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungById(id));
    }

    @GetMapping("/ueberweisungDate/{date}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungByDate(@PathVariable Date date) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungByDate(date));
    }

    @GetMapping("/ueberweisungVz/{id}/{verwendungszweck}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungByVerwendungszweck(@PathVariable Long id, @PathVariable String verwendungszweck) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungByVerwendungszweck(id,verwendungszweck));
    }

    @GetMapping("/ueberweisungSender/{id}/{sender}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungBySender(@PathVariable Long id, @PathVariable String sender) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungBySender(id, sender));
    }

    @GetMapping("/ueberweisungEmpfaenger/{id}/{empfaenger}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungByEmpfaenger(@PathVariable Long id, @PathVariable String empfaenger) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungByEmpfaenger(id, empfaenger));
    }

    @GetMapping("/ueberweisungEIban/{id}/{empfaengerIban}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungByEmpfaengerIban(@PathVariable Long id, @PathVariable String empfaengerIban) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungByEmpfaengerIban(id, empfaengerIban));
    }

    @GetMapping("/ueberweisungSIban/{senderIban}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungBySenderIban(@PathVariable String senderIban) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(this.ueberweisungService.getUeberweisungBySenderIban(senderIban));
    }

    @GetMapping("/ueberweisungByDate/{id}/{from}/{to}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungBetweenDates(@PathVariable Long id, @PathVariable String from, @PathVariable String to) throws UeberweisungNotFoundException {
        Timestamp date=Timestamp.valueOf(from);
        Timestamp date2=Timestamp.valueOf(to);
        return ResponseEntity.ok(ueberweisungService.getUeberweisungBetweenDates(id, date, date2));
    }

    @GetMapping("/ueberweisungByBetrag/{id}/{from}/{to}")
    public ResponseEntity<List<Ueberweisung>> getUeberweisungByBetrag(@PathVariable Long id, @PathVariable double from, @PathVariable double to) throws UeberweisungNotFoundException {
        return ResponseEntity.ok(ueberweisungService.getUeberweisungByBetrag(id, from, to));
    }

    @PostMapping("/einzahlung/{id}/{betrag}")
    public ResponseEntity<Ueberweisung> einzahlung(@PathVariable Long id, @PathVariable double betrag)throws KontoNotFoundException {
        return ResponseEntity.ok(ueberweisungService.einzahlung(id, betrag));
    }

    @PostMapping("/auszahlung/{id}/{betrag}")
    public ResponseEntity<Ueberweisung> auszahlung(@PathVariable Long id,@PathVariable double betrag)throws KontoNotFoundException {
        return ResponseEntity.ok(ueberweisungService.auszahlung(id, betrag));
    }
}

