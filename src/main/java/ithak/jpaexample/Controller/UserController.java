package ithak.jpaexample.Controller;


import ithak.jpaexample.Exceptions.BankNotFoundException;
import ithak.jpaexample.Exceptions.UserAlreadyExistsException;
import ithak.jpaexample.Exceptions.UserNotFoundException;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/user")
    public ResponseEntity<List<User>> getAllUser(){
        return ResponseEntity.ok(userService.getAllUser());
    }


    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) throws UserNotFoundException {
        return ResponseEntity.ok(this.userService.getUserById(id));
    }


    @PostMapping("/user")
    public ResponseEntity<User> addUser(@RequestBody User user) throws UserAlreadyExistsException {
        return ResponseEntity.ok(userService.addUser(user));
    }

    @DeleteMapping("/user/{id}/bank")
    public void deleteAllUserByBank(@PathVariable Long id) throws BankNotFoundException {
        userService.deleteAllUserByBank(id);
    }

    @PutMapping("/user")
    public ResponseEntity<User> updateUser(@RequestBody User user) throws UserNotFoundException {
        return ResponseEntity.ok(userService.updateUser(user));
    }

    @DeleteMapping("/user/{id}")
    public void deleteUserById(@PathVariable Long id) throws UserNotFoundException{
        userService.deleteUserById(id);
    }
}
