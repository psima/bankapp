package ithak.jpaexample.Controller;

import ithak.jpaexample.Exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@CrossOrigin(origins = "http://localhost:4200")
public class ExceptionController {
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleUserNotFound (UserNotFoundException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1000",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BankNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleBankNotFound (BankNotFoundException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1001",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(KontoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleKontoNotFound (KontoNotFoundException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1002",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UeberweisungDuplicateId.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleUeberweisungDuplicateId (UeberweisungDuplicateId ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1003",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(UeberweisungNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleUeberweisungNotFound (UeberweisungNotFoundException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1004",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UeberweisungNotValidException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleUeberweisungNotValid (UeberweisungNotValidException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1005",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleUserAlreadyExists (UserAlreadyExistsException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1006",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(DuplicateBankNameException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleDuplicateBankName (DuplicateBankNameException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1007",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

}