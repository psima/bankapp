package ithak.jpaexample.Controller;


import ithak.jpaexample.Exceptions.KontoNotFoundException;
import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Services.KontoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class KontoController {

    private KontoService kontoService;

    public KontoController(KontoService kontoService) {
        this.kontoService = kontoService;
    }

    @CrossOrigin
    @GetMapping("/konto/{username}")
    public ResponseEntity<Konto> getKontoWithUsername(@PathVariable String username) throws KontoNotFoundException {
        return ResponseEntity.ok(kontoService.getKontoWithUsername(username));}
}
