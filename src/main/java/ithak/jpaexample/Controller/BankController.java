package ithak.jpaexample.Controller;

import ithak.jpaexample.Exceptions.BankNotFoundException;
import ithak.jpaexample.Exceptions.DuplicateBankNameException;
import ithak.jpaexample.Exceptions.UserNotFoundException;
import ithak.jpaexample.Models.Bank;
import ithak.jpaexample.Models.User;

import ithak.jpaexample.Services.BankService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BankController {

    private BankService bankService;


    public BankController(BankService bankService) {

        this.bankService = bankService;

    }

    @PostMapping("/bank")
    public ResponseEntity<Bank> addBank(@RequestBody Bank bank) throws DuplicateBankNameException {
        return ResponseEntity.ok(bankService.addBank(bank));
    }

    @GetMapping("/bank")
    public ResponseEntity<List<Bank>> getAllBank() {
        return ResponseEntity.ok(bankService.getAllBank());
    }

    @GetMapping("/bank/{id}")
    public ResponseEntity<Bank> getBankById(@PathVariable Long id) throws BankNotFoundException {
        return ResponseEntity.ok(this.bankService.getBankById(id));
    }

    @PutMapping("/bank")
    public ResponseEntity<Bank> updateBank(@RequestBody Bank b) throws BankNotFoundException {
        return ResponseEntity.ok(this.bankService.updateBank(b));
    }

    @DeleteMapping("bank/{id}")
    public void deleteBankById(@PathVariable Long id) throws BankNotFoundException {
        bankService.deleteBankById(id);
    }

    @PostMapping("/bank/[id}/adduser")
    public  ResponseEntity<User> addKundeForBank(@PathVariable Long id, @RequestBody @Valid User u, BindingResult bindingResult) throws UserNotFoundException, BankNotFoundException {
        if (bindingResult.hasErrors()) {
            String message = "";
            for (FieldError fe : bindingResult.getFieldErrors()) {
                message += fe.getField() + ": " + fe.getDefaultMessage();
            }
            throw new UserNotFoundException();
        } else  {
            return ResponseEntity.ok(bankService.addKundeForBank(id, u));
        }
    }
}
