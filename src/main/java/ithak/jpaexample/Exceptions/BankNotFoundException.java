package ithak.jpaexample.Exceptions;

public class BankNotFoundException extends Exception{

    public BankNotFoundException(){
        super("Angefragte Bank nicht vorhanden");
    }
}
