package ithak.jpaexample.Exceptions;

public class UserNotFoundException extends Exception{

    public UserNotFoundException(){
        super("Angefragter Kunde existiert nicht.");
    }
}
