package ithak.jpaexample.Exceptions;

public class KontoNotFoundException  extends Exception{
    public  KontoNotFoundException() {super("Konto nicht vorhanden.");}
}
