package ithak.jpaexample.Exceptions;

public class UserAlreadyExistsException extends Exception{

    public UserAlreadyExistsException(){
        super("User existiert bereits.");
    }
}
