package ithak.jpaexample.Exceptions;

public class UeberweisungDuplicateId extends Exception {

    public UeberweisungDuplicateId(){
        super("ID der Ueberweisung bereits vorhanden.");
    }
}
