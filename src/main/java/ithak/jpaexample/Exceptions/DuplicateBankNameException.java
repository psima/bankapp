package ithak.jpaexample.Exceptions;

public class DuplicateBankNameException extends Exception {

    public DuplicateBankNameException(){
        super("Bankname bereits vorhanden.");
    }
}
