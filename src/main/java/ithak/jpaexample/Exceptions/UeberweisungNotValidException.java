package ithak.jpaexample.Exceptions;

public class UeberweisungNotValidException extends Exception{

    public UeberweisungNotValidException(String errorMessage) {
        super("Daten der Üeberweisung nicht korrekt: " + errorMessage);
    }
}
