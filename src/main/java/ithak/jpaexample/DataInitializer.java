package ithak.jpaexample;

import ithak.jpaexample.Models.Bank;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Repositories.BankRepository;
import ithak.jpaexample.Repositories.UserRepository;
import ithak.jpaexample.Services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Slf4j
public class DataInitializer{ //implements CommandLineRunner {


//    @Autowired
//    UserRepository users;
//
//    @Autowired
//    BankRepository bank;
//
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Bean
//    PasswordEncoder getEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Override
//    public void run(String... args ) throws Exception {
//
//        this.bank.save(Bank.builder()
//                .bankIban("AT000000002")
//                .bankName("test")
//                .guthaben(99999999)
//                .build()
//        );
//
//        //role models
//        this.users.save(User.builder()
//                .username("user")
//                .password(this.passwordEncoder.encode("password"))
//                .roles(Arrays.asList( "ROLE_USER"))
//                .verfuegernummer(1)
//                .build()
//        );
//
//        this.users.save(User.builder()
//                .username("admin")
//                .password(this.passwordEncoder.encode("password"))
//                .roles(Arrays.asList("ROLE_USER", "ROLE_ADMIN"))
//                .build()
//        );
//
//
//
//        log.debug("printing all users...");
//        this.users.findAll().forEach(v -> log.debug(" User :" + v.toString()));
//    }
}
