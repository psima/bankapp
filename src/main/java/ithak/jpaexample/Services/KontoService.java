package ithak.jpaexample.Services;

import ithak.jpaexample.Exceptions.KontoNotFoundException;
import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Repositories.KontoRepository;
import ithak.jpaexample.Repositories.UeberweisungRepository;
import ithak.jpaexample.Repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class KontoService {

    private KontoRepository kontoRepository;
    private UeberweisungRepository ueberweisungRepository;
    private UserRepository userRepository;

    public KontoService(KontoRepository kontoRepository, UeberweisungRepository ueberweisungRepository, UserRepository userRepository) {
        this.kontoRepository = kontoRepository;
        this.ueberweisungRepository = ueberweisungRepository;
        this.userRepository = userRepository;
    }

    public Konto createKonto(){
        Konto k = new Konto();
        k.setIban(createIban());

        return k;
    }

    private String createIban(){
        Random rand = new Random();
        String card = "AT";
        for (int i = 0; i < 14; i++)
        {
            int n = rand.nextInt(10) + 0;
            card += Integer.toString(n);
        }
        Optional<Konto> ou = kontoRepository.findKontoByIban(card);
        if (ou.isPresent()){
            createIban();
        }
        return card;
    }

    public Konto getKontoWithUsername(String username) throws KontoNotFoundException {
        Optional<User> ou = userRepository.findByUsername(username);

        if (ou.isPresent()){
            User u = ou.get();

            Optional<Konto> ok = kontoRepository.findKontoByUser(u);
            if (ok.isPresent()) {
                Konto k = ok.get();
                return k;
            } else {
                throw  new KontoNotFoundException();
            }
        } else{
            throw  new KontoNotFoundException();
        }
    }
}
