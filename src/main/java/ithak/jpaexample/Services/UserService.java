package ithak.jpaexample.Services;

import ithak.jpaexample.Exceptions.BankNotFoundException;
import ithak.jpaexample.Exceptions.UserAlreadyExistsException;
import ithak.jpaexample.Exceptions.UserNotFoundException;
import ithak.jpaexample.Models.Bank;
import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Repositories.BankRepository;
import ithak.jpaexample.Repositories.KontoRepository;
import ithak.jpaexample.Repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class UserService {

    private BankRepository bankRepository;
    private UserRepository userRepository;
    private KontoService kontoService;
    private KontoRepository kontoRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserService(KontoRepository kontoRepository, BankRepository bankRepository, UserRepository userRepository, KontoService kontoService) {
        this.bankRepository = bankRepository;
        this.userRepository = userRepository;
        this.kontoService = kontoService;
        this.kontoRepository = kontoRepository;
    }


   public List<User> getAllUser(){
        return userRepository.findAll();
   }

   public List<User> kundeByBank(Long id) throws UserNotFoundException {
        Optional<Bank> ob = bankRepository.findById(id);
        if (ob.isPresent()){
            return userRepository.findUserByBank(ob.get());
        } else {
            throw new UserNotFoundException();
        }
   }

    public User addUser(User user) throws UserAlreadyExistsException {
       if(userRepository.findByUsername(user.getUsername()).isPresent()){
           throw new UserAlreadyExistsException();
       } else {
           Bank b = bankRepository.findByBankName("IronBankOfBraavos");
           Konto k = kontoService.createKonto();

           user.setRoles(Arrays.asList("ROLE_USER"));
           user.setVerfuegernummer(createVerfuegernummer());
           user.setKonto(k);
           user.setBank(b);
           user.setPassword(passwordEncoder.encode(user.getPassword()));
           k.setUser(user);

           return  userRepository.save(user);
       }
    }

    public User getUserById(Long id) throws UserNotFoundException {
        Optional<User> ok = userRepository.findById(id);
        if (ok.isPresent()){
            return ok.get();
        } else {
            throw new UserNotFoundException();
        }
    }

    public void deleteAllUserByBank(Long id) throws BankNotFoundException {
        Optional<Bank> ob = bankRepository.findById(id);
        if (ob.isPresent()){
            Bank bank = ob.get();
            List<User> list = userRepository.findUserByBank(bank);

            for (User k : list){
                userRepository.delete(k);
            }
        } else {
            throw new BankNotFoundException();
        }
    }

    public User updateUser(User u) throws UserNotFoundException {
        if (u != null) {
            userRepository.save(u);
            return u;
        } else {
            throw new UserNotFoundException();
        }
    }

    public void deleteUserById(Long id) throws UserNotFoundException {
        Optional<User> ok = userRepository.findById(id);
        if (ok.isPresent()){
            User k = ok.get();
            userRepository.delete(k);

        } else {
            throw new UserNotFoundException();
        }
    }

    public int createVerfuegernummer(){
        int randomNum = ThreadLocalRandom.current().nextInt(1000000, 1999999 + 1);
        Optional<User> ou = userRepository.findUserByVerfuegernummer(randomNum);
        if (ou.isPresent()){
            createVerfuegernummer();
        }
        return randomNum;
    }



}
