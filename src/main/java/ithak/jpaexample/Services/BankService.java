package ithak.jpaexample.Services;

import ithak.jpaexample.Exceptions.BankNotFoundException;
import ithak.jpaexample.Exceptions.DuplicateBankNameException;
import ithak.jpaexample.Exceptions.UserNotFoundException;
import ithak.jpaexample.Models.Bank;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Repositories.BankRepository;
import ithak.jpaexample.Repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankService {

    BankRepository bankRepository;
    UserRepository userRepository;

    public BankService(BankRepository bankRepository, UserRepository userRepository) {
        this.bankRepository = bankRepository;
        this.userRepository = userRepository;
    }

    public List<Bank> getAllBank() {return bankRepository.findAll();}

    public Bank addBank(Bank bank) throws DuplicateBankNameException {
        if (bankRepository.findByBankName(bank.getBankName()) == null){
            return bankRepository.save(bank);
        } else {
            throw new DuplicateBankNameException();
        }
    }

    public Bank getBankById(Long id) throws BankNotFoundException {
        Optional<Bank> ob = bankRepository.findById(id);
        if (ob.isPresent()) {
            return ob.get();
        } else {
            throw new BankNotFoundException();
        }
    }

    public User addKundeForBank(Long id, User user) throws UserNotFoundException
    {
        Optional<Bank> ok = bankRepository.findById(id);
        if (ok.isPresent()){
            Bank bank = ok.get();
            user.setBank(bank);
            userRepository.save(user);
            return user;
        } else{
            throw new UserNotFoundException();
        }
    }

    public Bank updateBank(Bank b) throws BankNotFoundException {
        if (b != null) {
            bankRepository.save(b);

            return b;
        } else {
            throw new BankNotFoundException();
        }
    }

    public void deleteBankById(Long id) throws BankNotFoundException {
        Optional<Bank> ob = bankRepository.findById(id);
        if(ob.isPresent()){
            Bank b = ob.get();
            bankRepository.delete(b);
        } else {
            throw new BankNotFoundException();
        }
    }

}
