package ithak.jpaexample.Services;

import ithak.jpaexample.Exceptions.*;
import ithak.jpaexample.Models.Bank;
import ithak.jpaexample.Models.Konto;
import ithak.jpaexample.Models.Ueberweisung;
import ithak.jpaexample.Models.User;
import ithak.jpaexample.Repositories.BankRepository;
import ithak.jpaexample.Repositories.KontoRepository;
import ithak.jpaexample.Repositories.UeberweisungRepository;
import ithak.jpaexample.Repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UeberweisungService {

    private UeberweisungRepository ueberweisungRepository;
    private UserRepository userRepository;
    private KontoRepository kontoRepository;
    private BankRepository bankRepository;



    public UeberweisungService(UeberweisungRepository ueberweisungRepository, UserRepository userRepository, KontoRepository kontoRepository, BankRepository bankRepository) {
        this.ueberweisungRepository = ueberweisungRepository;
        this.userRepository = userRepository;
        this.kontoRepository = kontoRepository;
        this.bankRepository = bankRepository;
    }

    public List<Ueberweisung> getAllUeberweisung() {return ueberweisungRepository.findAll();}

    public Ueberweisung addUeberweisung(Ueberweisung ueberweisung) throws UeberweisungDuplicateId, KontoNotFoundException, UeberweisungNotValidException {
        String a = ueberweisung.getEmpfaengerIban();
        String b = ueberweisung.getSenderIban();

        Optional<Konto> em = kontoRepository.findKontoByIban(a);
        Optional<Konto> se = kontoRepository.findKontoByIban(b);

        if (ueberweisung.getId() != null){
            throw new UeberweisungDuplicateId();
        } else if (!(em.isPresent()) || !(se.isPresent())){
            throw new KontoNotFoundException();
        }
        else {
            Konto oa = em.get();
            Konto ob = se.get();
            ueberweisung.setEmpfaengerKonto(oa);
            ueberweisung.setSenderKonto(ob);
            oa.setGuthaben((oa.getGuthaben() + ueberweisung.getBetrag()));
            ob.setGuthaben((ob.getGuthaben() - ueberweisung.getBetrag()));

            kontoRepository.save(oa);
            kontoRepository.save(ob);
            ueberweisungRepository.save(ueberweisung);
            return ueberweisung;
        }
    }

    private boolean checkForId(Ueberweisung ueberweisung) {
        boolean b = false;
        Optional<Ueberweisung> ou = ueberweisungRepository.findById(ueberweisung.getId());
        if (ou.isPresent()){
            b = true;
            return b;
        } else {
            return b;
        }
    }

    public Ueberweisung getUeberweisungById(Long id) throws UeberweisungNotFoundException {
        Optional<Ueberweisung> ou = ueberweisungRepository.findById(id);
        if (ou.isPresent()) {
            return ou.get();
        } else {
            throw new UeberweisungNotFoundException();
        }
    }

   public List<Ueberweisung> getUeberweisungByDate(Date date) throws UeberweisungNotFoundException {
        List<Ueberweisung> ou = ueberweisungRepository.findUeberweisungByDatum(date);
        if (!ou.isEmpty()){
            return ou;
        } else {
            throw new UeberweisungNotFoundException();
        }
   }

   public List<Ueberweisung> getUeberweisungByVerwendungszweck(Long id, String verwendungszweck) throws UeberweisungNotFoundException {
       List<Ueberweisung> temp = ueberweisungRepository.findAll();
       List<Ueberweisung> ueberweisungWithId = new ArrayList<>();

       if (temp != null) {
           for (Ueberweisung t : temp) {
               if (t.getEmpfaengerKonto().getId() == id || t.getSenderKonto().getId() == id) {
                   ueberweisungWithId.add(t);
               }
           }
       }
       else {
           throw new UeberweisungNotFoundException();
       }

       List<Ueberweisung> textList = new ArrayList<>();
       verwendungszweck = verwendungszweck.toLowerCase().trim();

       for (Ueberweisung t : ueberweisungWithId) {

           if(t.getVerwendungszweck().toLowerCase().trim().contains(verwendungszweck)){
               textList.add(t);
           }

       }
       return textList;
   }

   public List<Ueberweisung> getUeberweisungBySender(Long id, String sender) throws UeberweisungNotFoundException {
       List<Ueberweisung> temp = ueberweisungRepository.findAll();
       List<Ueberweisung> ueberweisungWithId = new ArrayList<>();

       if (temp != null) {
           for (Ueberweisung t : temp) {
               if (t.getSenderKonto().getId() == id) {
                   ueberweisungWithId.add(t);
               }
           }
       }
       else {
           throw new UeberweisungNotFoundException();
       }

       List<Ueberweisung> textList = new ArrayList<>();
       sender = sender.toLowerCase().trim();

       for (Ueberweisung t : ueberweisungWithId) {

           if(t.getSender().toLowerCase().trim().contains(sender)){
               textList.add(t);
           }

       }
       return textList;
   }

    public List<Ueberweisung> getUeberweisungByEmpfaenger(Long id, String empfaenger) throws UeberweisungNotFoundException {
        List<Ueberweisung> temp = ueberweisungRepository.findAll();
        List<Ueberweisung> ueberweisungWithId = new ArrayList<>();

        if (temp != null) {
            for (Ueberweisung t : temp) {
                if (t.getEmpfaengerKonto().getId() == id) {
                    ueberweisungWithId.add(t);
                }
            }
        }
        else {
            throw new UeberweisungNotFoundException();
        }

        List<Ueberweisung> textList = new ArrayList<>();
        empfaenger = empfaenger.toLowerCase().trim();

        for (Ueberweisung t : ueberweisungWithId) {

            if(t.getEmpfaenger().toLowerCase().trim().contains(empfaenger)){
                textList.add(t);
            }

        }
        return textList;
    }

    public List<Ueberweisung> getUeberweisungByEmpfaengerIban(Long id, String empfaengerIban) throws UeberweisungNotFoundException {
        List<Ueberweisung> temp = ueberweisungRepository.findAll();
        List<Ueberweisung> ueberweisungWithId = new ArrayList<>();

        if (temp != null) {
            for (Ueberweisung t : temp) {
                if (t.getSenderKonto().getId() == id) {
                    ueberweisungWithId.add(t);
                }
            }
        }
        else {
            throw new UeberweisungNotFoundException();
        }

        List<Ueberweisung> textList = new ArrayList<>();
        empfaengerIban = empfaengerIban.toLowerCase().trim();

        for (Ueberweisung t : ueberweisungWithId) {

            if(t.getEmpfaengerIban().toLowerCase().trim().contains(empfaengerIban)){
                textList.add(t);
            }

        }
        return textList;
    }

    public List<Ueberweisung> getUeberweisungBySenderIban(String senderIban) throws UeberweisungNotFoundException {
        List<Ueberweisung> temp = ueberweisungRepository.findAll();
        List<Ueberweisung> ueberweisungWithId = new ArrayList<>();

        if (temp != null) {
            for (Ueberweisung t : temp) {
                    ueberweisungWithId.add(t);
                }
            }
        else {
            throw new UeberweisungNotFoundException();
        }

        List<Ueberweisung> textList = new ArrayList<>();
        senderIban = senderIban.toLowerCase().trim();

        for (Ueberweisung t : ueberweisungWithId) {

            if(t.getEmpfaengerIban().toLowerCase().trim().contains(senderIban)){
                textList.add(t);
            }

        }
        return textList;
    }

    public List<Ueberweisung> getUeberweisungBetweenDates(Long id, Date date, Date date2) throws UeberweisungNotFoundException {
        List<Ueberweisung> temp = ueberweisungRepository.findUeberweisungByDatumBetween(date, date2);

        List<Ueberweisung> ueberweisungWithId=new ArrayList<>();

        if (temp != null) {
            for (Ueberweisung t : temp) {

                if (t.getEmpfaengerKonto().getId() == id || t.getSenderKonto().getId() == id) {
                    ueberweisungWithId.add(t);
                }
            }
        }
        else {
            throw new UeberweisungNotFoundException();
        }
        return ueberweisungWithId;
    }

    public List<Ueberweisung> getUeberweisungByBetrag(Long id, double from, double to) throws UeberweisungNotFoundException {
        List<Ueberweisung> temp = ueberweisungRepository.findAll();
        List<Ueberweisung> ueberweisungWithId=new ArrayList<>();

        if(temp!=null) {

            for (Ueberweisung t : temp) {

                if (t.getEmpfaengerKonto().getId() == id || t.getSenderKonto().getId() == id) {
                    ueberweisungWithId.add(t);
                }

            }
        }
        else{
            throw new UeberweisungNotFoundException();
        }

        List<Ueberweisung> fromAndToAmount=new ArrayList<>();

        for (Ueberweisung t : ueberweisungWithId) {
            if(t.getBetrag()>= from && t.getBetrag()<= to){
                fromAndToAmount.add(t);
            }

        }
        return fromAndToAmount;
    }

    public Ueberweisung einzahlung(Long id, double betrag) throws KontoNotFoundException {
        Optional<Konto> ok = kontoRepository.findById(id);

        if(ok.isPresent()){
            Konto k = ok.get();
            User user = userRepository.findUserByKonto(k);
            Ueberweisung u = new Ueberweisung();
            u.setSenderKonto(k);
            u.setEmpfaengerKonto(k);
            u.setBetrag(betrag);
            u.setVerwendungszweck("Bareinzahlung");
            u.setSender("");
            u.setEmpfaenger(user.getUsername());
            u.setEmpfaengerIban(k.getIban());
            u.setSenderIban(k.getIban());
            k.setGuthaben((k.getGuthaben()+betrag));

            Bank b = user.getBank();
            b.setGuthaben((b.getGuthaben()+betrag));

            ueberweisungRepository.save(u);
            kontoRepository.save(k);
            bankRepository.save(b);

            System.out.println("Bareinzahlung");
            System.out.println();
            System.out.println("Kunde: " + user.getUsername());
            System.out.println("Betrag: €" + u.getBetrag());
            System.out.println("Datum: " + u.getDatum());
            System.out.println();
            System.out.println("The Iron Bank will have it´s due.");

            return u;
        } else {
            throw new KontoNotFoundException();
        }
    }

    public Ueberweisung auszahlung(Long id, double betrag) throws KontoNotFoundException {
        Optional<Konto> ok = kontoRepository.findById(id);

        if(ok.isPresent()){
            Konto k = ok.get();
            User user = userRepository.findUserByKonto(k);
            Ueberweisung u = new Ueberweisung();
            u.setSenderKonto(k);
            u.setEmpfaengerKonto(k);
            u.setBetrag(betrag);
            u.setVerwendungszweck("Barauszahlung");
            u.setSender("");
            u.setEmpfaenger(user.getUsername());
            u.setEmpfaengerIban(k.getIban());
            u.setSenderIban(k.getIban());
            k.setGuthaben((k.getGuthaben()-betrag));

            Bank b = user.getBank();
            b.setGuthaben((b.getGuthaben()-betrag));

            ueberweisungRepository.save(u);
            kontoRepository.save(k);
            bankRepository.save(b);

            System.out.println("Barauszahlung");
            System.out.println();
            System.out.println("Kunde: " + user.getUsername());
            System.out.println("Betrag: €" + u.getBetrag());
            System.out.println("Datum: " + u.getDatum());
            System.out.println();
            System.out.println("The Iron Bank will have it´s due.");

            return u;
        } else {
            throw new KontoNotFoundException();
        }
    }

}
